import { createContext } from 'react'
import game from '../store/Game'

const GameContext = createContext()

export const GameProvider = ({ children }) => {
  return <GameContext.Provider value={game}> {children}</GameContext.Provider>
}

export default GameContext
