import { createContext, useEffect, useState } from 'react'
import socket from '../sockets/socket'
const AuthContext = createContext({})

export const AuthProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(true)
  const [user, setUser] = useState(null)

  const userInfo = JSON.parse(localStorage.getItem('user'))

  useEffect(() => {
    if (userInfo) {
      setUser(userInfo)
      socket.join(userInfo)
    } else {
      setUser(null)
    }
    setIsLoading(false)
    // eslint-disable-next-line
  }, [])

  return (
    <AuthContext.Provider
      value={{
        isLoading,
        user,
        setUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
export default AuthContext
