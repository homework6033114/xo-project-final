import { io } from 'socket.io-client'
import gameService from '../API/GameService'
import game from '../store/Game'

class Socket {
  constructor() {
    this.socket = io.connect('http://localhost:5000')
    this.socket.on('message', (msg) => console.log(msg))
    this.socket.on('players:invited', ({ fromPlayer, toPlayer, message }) => {
      if (window.confirm(message)) {
        socket.accept(fromPlayer, toPlayer)
      } else {
        socket.decline(fromPlayer, toPlayer)
      }
    })

    this.socket.on(
      'players:accepted',
      ({ fromPlayer: player2, toPlayer: player1, message }) => {
        console.log(message)
        socket.joinGame(player1, player2)
      }
    )

    this.socket.on('players:declined', ({ message }) => alert(message))

    this.socket.on('game:joined', ({ gameId }) => {
      window.location.replace(`/games/${gameId}`)
    })

    this.socket.on('game:move-made', ({ position, symbol }) => {
      game.moveMade(position, symbol)
      if (game.isOver) {
        const updateGame = async (data, gameId) => {
          return await gameService.updateGame(data, gameId)
        }
        const resultOpt = { O: true, X: false, null: null }
        const gameData = {
          gameResult: resultOpt[game.winner],
          gameEnd: new Date().toUTCString(),
        }
        updateGame(gameData, game.gameId)
      }
    })
  }

  join(user) {
    this.socket.emit('players:join', { user })
  }

  invite(fromPlayer, toPlayer) {
    this.socket.emit('players:invite', {
      fromPlayer: fromPlayer.id,
      toPlayer: toPlayer,
      message: `Игрок ${fromPlayer.userName} приглашает вас в игру`,
    })
  }

  accept(fromPlayer, toPlayer) {
    this.socket.emit('players:accept', {
      fromPlayer,
      toPlayer,
      message: `Игрок принял ваше приглашение`,
    })
  }

  decline(fromPlayer, toPlayer) {
    this.socket.emit('players:decline', {
      toPlayer: toPlayer,
      message: `Игрок отклонил ваше приглашение`,
    })
  }

  async joinGame(idPlayer1, idPlayer2) {
    const gameId = await gameService.createGame({
      idPlayer1,
      idPlayer2,
      gameBegin: new Date().toUTCString(),
    })
    this.socket.emit('game:join', {
      gameId,
      toPlayer: idPlayer2,
    })
    window.location.replace(`/games/${gameId}`)
  }

  startGame(gameId, player1, player2) {
    this.socket.emit(
      'game:start',
      {
        room: gameId,
      },
      (cb) =>
        console.log(
          `${player1.userName} и ${player2.userName} присоединились к игре #${cb}`
        )
    )
  }

  makeMove(gameId, position, symbol) {
    this.socket.emit('game:make-move', {
      gameId,
      position,
      symbol,
    })
  }
  sendMessage(message) {
    this.socket.emit('game:message', {
      message,
    })
  }
}

const socket = new Socket()
export default socket
