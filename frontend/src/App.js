import { Routes, Route } from 'react-router-dom'
import Login from './pages/Login'
import GamePage from './pages/GamePage'
import Players from './pages/Players'
import Rating from './pages/Rating'
import History from './pages/History'
import Users from './pages/Users'
import PrivateRoute from './components/PrivateRoute'
import AdminRoute from './components/AdminRoute'
import { AuthProvider } from './context/AuthContext'
import { GameProvider } from './context/GameContext'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const App = () => {
  return (
    <>
      <ToastContainer />
      <AuthProvider>
        <GameProvider>
          <Routes>
            <Route path="/" element={<Login />} />
            {/* <Route path="/auth" element={<Login />} /> */}
            <Route
              path="/users"
              element={
                <PrivateRoute>
                  <Players />
                </PrivateRoute>
              }
            />
            <Route
              path="/users/rating"
              element={
                <PrivateRoute>
                  <Rating />
                </PrivateRoute>
              }
            />
            <Route
              path="/games/:gameId"
              element={
                <PrivateRoute>
                  <GamePage />
                </PrivateRoute>
              }
            />
            <Route
              path="/games/history"
              element={
                <PrivateRoute>
                  <History />
                </PrivateRoute>
              }
            />
            <Route
              path="/admin/users"
              element={
                <AdminRoute>
                  <Users />
                </AdminRoute>
              }
            />
          </Routes>
        </GameProvider>
      </AuthProvider>
    </>
  )
}

export default App
