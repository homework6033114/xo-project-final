const players = [
  {
    id: 1,
    userName: 'Плюшкина Екатерина Викторовна',
    mode: 'X',
    winRate: '23',
    isCurrentUser: true,
  },

  {
    id: 2,
    userName: 'Пупкин Владлен Игоревич',
    mode: 'O',
    winRate: '63%',
    isCurrentUser: false,
  },
]

const [player1, player2] = players

export const chat = [
  {
    id: 1,
    player: player1,
    userName: player1.userName,
    text: '!Ну что, готовься к поражению!!1',
    time: '13:40',
  },
  {
    id: 2,
    player: player2,
    userName: player2.userName,
    text: 'Надо было играть за крестики. Розовый — мой не самый счастливый цвет',
    time: '13:41',
  },
  {
    id: 3,

    player: player1,
    userName: player1.userName,

    text: 'Ну что, готовься к поражению!!1',
    time: '13:40',
  },
  {
    id: 4,
    player: player2,
    userName: player2.userName,
    text: 'Надо было играть за крестики. Розовый — мой не самый счастливый цвет',
    time: '13:41',
  },
  {
    id: 5,
    player: player1,
    userName: player1.userName,
    text: 'Ну что, готовься к поражению!!1',
    time: '13:40',
  },
  {
    id: 6,
    player: player2,
    userName: player2.userName,
    text: 'Надо было играть за крестики. Розовый — мой не самый счастливый цвет',
    time: '13:41',
  },
  {
    id: 7,
    player: player1,
    userName: player1.userName,
    text: 'Ну что, готовься к поражению!!1',
    time: '13:40',
  },
  {
    id: 8,
    player: player2,
    userName: player2.userName,
    text: 'Надо было играть за крестики. Розовый — мой не самый счастливый цвет',
    mode: '0',

    time: '13:41',
  },
  {
    id: 9,
    player: player2,
    userName: player2.userName,
    text: 'Надо было играть за крестики. Розовый — мой не самый счастливый цвет',

    time: '13:41',
  },
  {
    id: 10,
    player: player1,
    userName: player1.userName,
    text: 'Ну что, готовься к поражению!!1',

    time: '13:40',
  },
  {
    id: 11,
    player: player2,
    userName: player2.userName,
    text: 'Надо было играть за крестики. Розовый — мой не самый счастливый цвет ',

    time: '13:41',
  },
]
