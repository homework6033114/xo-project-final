import axios from 'axios'
import { toast } from 'react-toastify'

const API_URL = '/api/auth/'

// Авторизация пользователя
const authorize = async (userData) => {
  const response = await axios.post(API_URL, userData)
  return response.data
}

// Logout user
const logout = async () => {
  try {
    const response = await axios.post(API_URL + 'logout')
    toast.success(response.data.message)
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

// Регистрация нового пользователя
const register = async (userId, userData) => {
  try {
    const response = await axios.post(API_URL + 'register/' + userId, userData)
    toast.success(response.data.message)
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

const authService = {
  authorize,
  logout,
  register,
}

export default authService
