import axios from 'axios'
import { toast } from 'react-toastify'

const API_URL = '/api/users/'

// Получение списка игроков
const getUsers = async () => {
  try {
    const response = await axios.get(API_URL)
    return response.data
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

// Получение списка активных игроков
const getPlayers = async () => {
  try {
    const response = await axios.get(API_URL + 'active')
    return response.data
  } catch (error) {
    toast.error(error.response.data.message)
  }
}
// Получение рейтинга игроков
const getRating = async () => {
  try {
    const response = await axios.get(API_URL + 'rating')
    return response.data
  } catch (error) {
    toast.error(error.response.data.message)
  }
}
// Получение рейтинга одного игрока
const getPlayerRating = async (userId) => {
  try {
    const response = await axios.get(API_URL + 'rating/' + userId)
    return response.data[0].wins_perc
  } catch (error) {
    toast.error(error.response.data.message)
  }
}
//  Добавление нового пользователя
const addUser = async (data) => {
  try {
    const response = await axios.post(API_URL, data)
    toast.success(response.data.message)
  } catch (error) {
    toast.error(error.response.data.message)
  }
}
// Обновление данных пользователя
const updateUser = async (userId, data) => {
  try {
    const response = await axios.put(API_URL + userId, data)
    toast.success(response.data.message)
  } catch (error) {
    toast.error(error.response.data.message)
  }
}
const usersService = {
  getUsers,
  getPlayers,
  getRating,
  getPlayerRating,
  addUser,
  updateUser,
}

export default usersService
