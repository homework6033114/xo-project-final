import axios from 'axios'
import { toast } from 'react-toastify'

const API_URL = '/api/games/'

// Получение списка игр
const getGames = async () => {
  try {
    const response = await axios.get(API_URL)
    return response.data
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

// Создание новой игры
const createGame = async (gameData) => {
  try {
    const response = await axios.post(API_URL, gameData)
    return response.data[0].id
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

// Получение информации о текущей игре
const getGame = async (gameId) => {
  try {
    const response = await axios.get(API_URL + gameId)
    return response.data[0]
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

// @desc  Обновление игры
const updateGame = async (gameData, gameId) => {
  try {
    const response = await axios.put(API_URL + gameId, gameData)
    console.log(response)
    // toast.success(response.data.message)
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

// Получение данных игрового поля
const getMoves = async (gameId) => {
  try {
    const response = await axios.get(API_URL + gameId + '/moves')
    return response.data
  } catch (error) {
    toast.error(error.response.data.message)
  }
}
// Создание сообщения в чате
const addMessage = async (message, gameId) => {
  try {
    const response = await axios.post(API_URL + gameId + '/chat', message)
    return response.data
  } catch (error) {
    toast.error(error.response.data.message)
  }
}
// Получение сообщений из чата
const getMessages = async (gameId) => {
  try {
    const response = await axios.get(API_URL + gameId + '/chat')
    return response.data
  } catch (error) {
    toast.error(error.response.data.message)
  }
}

const gameService = {
  createGame,
  updateGame,
  getGame,
  getGames,
  getMoves,
  addMessage,
  getMessages,
}

export default gameService
