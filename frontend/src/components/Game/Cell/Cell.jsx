import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import Symbol from '../../UI/Symbol/Symbol'
import './Cell.css'
import GameContext from '../../../context/GameContext'
import AuthContext from '../../../context/AuthContext'

const Cell = ({ onClick, i }) => {
  const game = useContext(GameContext)
  const { user } = useContext(AuthContext)
  const symbol = game.field[i]

  const getWinComb = (cells) => {
    const combs = [
      // Rows
      ['0', '1', '2'],
      ['3', '4', '5'],
      ['6', '7', '8'],
      // Columns
      ['0', '3', '6'],
      ['1', '4', '7'],
      ['2', '5', '8'],
      // Diagonal
      ['0', '4', '8'],
      ['2', '4', '6'],
    ]
    for (let comb of combs) {
      if (
        cells[comb[0]] !== null &&
        cells[comb[0]] === cells[comb[1]] &&
        cells[comb[1]] === cells[comb[2]]
      ) {
        return [+comb[0], +comb[1], +comb[2]]
      }
    }
  }
  const winComb = getWinComb(game.field)
  const isWinCell = game.winner && winComb.includes(i)
  const isNotAllowed = (i) => {
    if (game.field[i] || game.roles[game.mode] !== user.id) {
      return 'blocked'
    }
  }

  const getWinCellClass = (i) => {
    if (isWinCell) {
      if (symbol === 'X') {
        return 'winX'
      } else if (symbol === 'O') {
        return 'winO'
      }
    }
  }
  const winCellClass = getWinCellClass(i)
  const blockedCellClass = isNotAllowed(i)
  return (
    <div
      className={`cell ${winCellClass} ${blockedCellClass}`}
      onClick={onClick}
      data-cell-index={i}
    >
      <Symbol symbol={symbol} />
    </div>
  )
}

export default observer(Cell)
