import React, { useState, useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { reaction } from 'mobx'
import { observer } from 'mobx-react-lite'
import Field from './Field'
import SymbolSmall from '../UI/Symbol/SymbolSmall'
import WinnerModal from '../Modals/WinnerModal'
import Timer from './Timer'
import GameContext from '../../context/GameContext'
import AuthContext from '../../context/AuthContext'
import gameService from '../../API/GameService'
import socket from '../../sockets/socket'
import './Game.css'

const GameBoard = () => {
  const game = useContext(GameContext)
  const { user: currentUser } = useContext(AuthContext)
  const { gameId } = useParams()
  const [player1, player2] = game.players
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [time, setTime] = useState(0)
  // const [isRunning, setIsRunning] = useState(true)

  // useEffect(() => {
  //   let intervalId
  //   if (isRunning) {
  //     intervalId = setInterval(() => setTime(time + 1), 10)
  //   }
  //   return () => clearInterval(intervalId)
  // }, [isRunning, time])

  // const resetTimer = () => {
  //   setTime(0)
  //   setIsRunning(true)
  // }

  useEffect(() => {
    socket.startGame(gameId, player1, player2)
    const getGameMoves = async (id) => {
      const gameMoves = await gameService.getMoves(id)
      let field = [null, null, null, null, null, null, null, null, null]
      for (let i = 0; i < gameMoves.length; i++) {
        field[gameMoves[i].position] = gameMoves[i].symbol
      }
      game.setField(field)
    }

    getGameMoves(gameId)
  }, [gameId, player1, player2])

  const play = (cell) => {
    if (game.roles[game.mode] === currentUser.id) {
      socket.makeMove(game.gameId, cell, game.mode)
    }
  }

  const playAgain = () => {
    const fromPlayer = currentUser
    const toPlayer = currentUser.id === player1.id ? player2.id : player1.id
    socket.invite(fromPlayer, toPlayer)
  }

  reaction(
    () => game.isOver,
    () => {
      setModalIsOpen(true)
    }
  )

  return (
    <>
      <div className="game-container">
        <Timer time={time} />
        <Field play={play} />
        <div className="game-step">
          Ходит&nbsp;
          <SymbolSmall symbol={game.mode} />
          &nbsp;
          {game.mode === 'X'
            ? `${player1.firstName} ${player1.surName}`
            : `${player2.firstName} ${player2.surName}`}
        </div>
      </div>
      {game.isOver && (
        <WinnerModal
          modalIsOpen={modalIsOpen}
          playAgain={playAgain}
          setModalIsOpen={setModalIsOpen}
        />
      )}
    </>
  )
}

export default observer(GameBoard)
