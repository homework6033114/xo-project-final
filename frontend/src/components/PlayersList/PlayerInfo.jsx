import React from 'react'
import './PlayersList.css'

const PlayerInfo = ({ name, winRate }) => {
  return (
    <div className="subject-info">
      <div className="subject-name">{name}</div>
      <div className="subject-percent">{winRate}% побед</div>
    </div>
  )
}

export default PlayerInfo
