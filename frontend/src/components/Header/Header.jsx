import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import Tab from '../UI/Tab/Tab'
import Button from '../UI/Button/Button'
import './Header.css'
import AuthContext from '../../context/AuthContext'
import game from '../../store/Game'
import authService from '../../API/AuthService'

const Header = () => {
  const { user: currentUser } = useContext(AuthContext)
  const navigate = useNavigate()

  const onLogout = async () => {
    localStorage.removeItem('user')
    await authService.logout()
    navigate('/auth')
  }
  return (
    <header>
      <div className="logo">
        <img src="../imgs/s-logo.svg" alt="logo" />
      </div>
      <div className="nav-panel">
        {game.gameId && <Tab path={`/games/${game.gameId}`}>Игровое поле</Tab>}
        <Tab path="/users/rating">Рейтинг</Tab>
        <Tab path="/users">Активные игроки</Tab>
        <Tab path="/games/history">История игр</Tab>
        {currentUser.isAdmin && <Tab path="/admin/users">Список игроков</Tab>}
      </div>
      <Button version="icon" onClick={onLogout}>
        <img src="../imgs/signout-icon.svg" alt="sign-up button" />
      </Button>
    </header>
  )
}

export default Header
