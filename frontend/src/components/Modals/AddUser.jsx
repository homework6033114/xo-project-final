import React, { useState } from 'react'
import './Modal.css'
import Title from '../UI/Text/Title'
import Input from '../UI/Input/Input'
import Button from '../UI/Button/Button'

const AddUserModal = ({ onClose, modalIsOpen, setModalIsOpen, addUser }) => {
  const [userData, setUserData] = useState({
    userName: '',
    age: '',
    gender: '',
  })
  const [radioButtons, setRadioButtons] = useState({
    male: false,
    female: false,
  })

  const { userName, age, gender } = userData
  const { male, female } = radioButtons

  const onChange = (e) => {
    setUserData((prevState) => ({
      ...prevState,
      [e.target.id]: e.target.value,
    }))
  }
  const onCheck = (e) => {
    let gender = e.target.id === 'male' ? 'M' : 'Ж'
    setRadioButtons((prevState) => ({
      ...prevState,
      [e.target.id]: true,
    }))

    setUserData((prevState) => ({
      ...prevState,
      gender,
    }))
  }

  const resetValues = () => {
    setUserData({
      userName: '',
      age: '',
      gender: '',
    })
    setRadioButtons({
      male: false,
      female: false,
    })
  }

  const onSubmit = (e) => {
    e.preventDefault()
    if (!userName || !age || !gender) {
      return
    }
    addUser(userData)

    setModalIsOpen(false)
    resetValues()
  }

  return (
    <div className={`backdrop ${modalIsOpen ? 'close' : 'open'}`}>
      <div className="modalContainer">
        <div className="modal modal-addUser">
          <div className="btn-close">
            <div onClick={onClose}>
              <img
                className="icon"
                src="../imgs/btn-close.svg"
                alt="btn-close"
              />
            </div>
          </div>
          <Title>Добавьте игрока</Title>
          <form onSubmit={onSubmit}>
            <div className="input-set name">
              <p className="label">ФИO</p>
              <Input
                type="text"
                placeholder="Иванов Иван Иванович"
                id="userName"
                isValid={true}
                value={userName}
                onChange={onChange}
              />
            </div>
            <div className="input-row">
              <div className="input-set age">
                <p className="label">Возраст</p>
                <Input
                  type="number"
                  id="age"
                  isValid={true}
                  onChange={onChange}
                  value={age}
                  placeholder="0"
                />
              </div>
              <div className="input-set gender">
                <p className="label">Пол</p>
                <div className="radio-set">
                  <input
                    checked={female}
                    type="radio"
                    id="female"
                    className="radio-button"
                    onChange={onCheck}
                  />
                  <label htmlFor="female">
                    <img src="../imgs/female.svg" alt="gender icon" />
                  </label>
                  <input
                    checked={male}
                    type="radio"
                    id="male"
                    className="radio-button"
                    onChange={onCheck}
                  />
                  <label htmlFor="male">
                    <img src="../imgs/male.svg" alt="gender icon" />
                  </label>
                </div>
              </div>
            </div>

            <Button version="primary" type="Submit">
              Добавить
            </Button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AddUserModal
