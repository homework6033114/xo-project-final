import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import MessageContainer from './MessageContainer/MessageContainer'
import Input from '../UI/Input/Input'
import Button from '../UI/Button/Button'
import './Chat.css'
import GameContext from '../../context/GameContext'
import AuthContext from '../../context/AuthContext'
import gameService from '../../API/GameService'
import socket from '../../sockets/socket'

const Chat = () => {
  const game = useContext(GameContext)
  const { user: currentUser } = useContext(AuthContext)
  const { gameId } = useParams()
  const [text, setText] = useState('')

  useEffect(() => {
    const getMessages = async (id) => {
      const data = await gameService.getMessages(id)
      if (data) {
        game.setMessages(data)
      }
    }
    getMessages(gameId)
    // eslint-disable-next-line
  }, [game.messages, gameId])

  const onChange = (e) => {
    setText(e.target.value)
  }

  const sendMessage = async (e) => {
    e.preventDefault()

    const newMessage = {
      idGame: game.gameId,
      idUser: currentUser.id,
      message: text,
      createdAt: new Date().toUTCString(),
    }

    socket.sendMessage(newMessage)

    setText('')
  }
  return (
    <div className="chat-container">
      <div className="scrollable-wrapper">
        <div className="msgs-container">
          {game.messages.length === 0 && (
            <div className="text">Сообщений еще нет</div>
          )}
          {game.messages.map((message) => (
            <MessageContainer key={message.createdAt} chat={message} />
          ))}
        </div>
      </div>
      <div className="msg-interactive-elements">
        <Input
          type="text"
          value={text}
          placeholder="Сообщение..."
          isValid={true}
          onChange={onChange}
        />
        <Button version="primary" onClick={sendMessage}>
          <img src="../imgs/send-btn.svg" alt="кнопка отправить" />
        </Button>
      </div>
    </div>
  )
}

export default observer(Chat)
