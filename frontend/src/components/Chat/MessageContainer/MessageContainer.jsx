import React, { useContext } from 'react'
import './MessageContainer.css'
import game from '../../../store/Game'
import AuthContext from '../../../context/AuthContext'
import { formatTime } from '../../../utils/formatValue'

const MessageContainer = ({ chat }) => {
  const { createdAt, message, idUser } = chat
  const { user: currentUser } = useContext(AuthContext)
  const [player1, player2] = game.players
  const user = idUser === player1.id ? player1 : player2

  return (
    <div
      className={`msg-container ${idUser === currentUser.id ? 'me' : 'other'}`}
    >
      <div className="msg-header">
        <div
          className={`subject-name ${
            idUser === game.roles['X'] ? 'x' : 'zero'
          }`}
        >
          {user.surName} {user.firstName}
        </div>
        <div className="time">{formatTime(createdAt)}</div>
      </div>
      <div className="msg-body">{message}</div>
    </div>
  )
}

export default MessageContainer
