import React from 'react'
import './Input.css'

const Input = ({
  type,
  name,
  id,
  placeholder,
  isValid,
  value,
  onPaste,
  onCopy,
  onChange,
}) => {
  return (
    <input
      className={`input ${!isValid ? 'error' : ''}`}
      type={type}
      name={name}
      id={id}
      value={value}
      placeholder={placeholder}
      onPaste={onPaste}
      onCopy={onCopy}
      onChange={onChange}
    />
  )
}

export default Input
