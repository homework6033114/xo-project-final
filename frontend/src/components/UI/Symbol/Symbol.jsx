import React from 'react'

const Symbol = ({ symbol }) => {
  if (symbol === 'X') {
    return <img src="../imgs/xxl-x.svg" alt="X symbol" />
  } else if (symbol === 'O') {
    return <img src="../imgs/xxl-zero.svg" alt="zero symbol" />
  } else {
    return null
  }
}

export default Symbol
