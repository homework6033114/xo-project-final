import React from 'react'
import './Status.css'

const StatusM = ({ status, children }) => {
  return <div className={`status status-M ${status}`}>{children}</div>
  // return <div className={`status status-M ${status}`} />
}

export default StatusM
