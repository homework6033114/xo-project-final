import React from 'react'
import { NavLink } from 'react-router-dom'

import './Tab.css'

const Tab = ({ isActive, path, children }) => {
  return (
    <div className="tab">
      <NavLink end to={path} className={isActive ? 'active' : ''}>
        {children}
      </NavLink>
    </div>
  )
}

export default Tab
