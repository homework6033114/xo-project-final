import React from 'react'
import './SwitchToggle.css'

const SwitchToggle = ({ onChange, isChecked }) => {
  return (
    <label htmlFor="switch" className="switch">
      <input
        type="checkbox"
        id="switch"
        onChange={onChange}
        checked={isChecked}
      />
      <span className="slider"></span>
    </label>
  )
}

export default SwitchToggle
