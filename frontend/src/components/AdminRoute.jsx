import { useContext } from 'react'
import { Navigate } from 'react-router-dom'
import AuthContext from '../context/AuthContext'
import Spinner from './UI/Spinner/Spinner'

const AdminRoute = ({ children }) => {
  const { user, isLoading } = useContext(AuthContext)
  if (isLoading) return <Spinner />

  if (user && user.isAdmin) {
    return children
  } else {
    return <Navigate to="/auth" />
  }
}

export default AdminRoute
