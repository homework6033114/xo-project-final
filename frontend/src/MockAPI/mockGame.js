import { mockPlayers } from '../dev-data/mockPlayers'
import game from '../modules/Game'

const mockApiCall = (
  data,
  { customErrors = [], errorRate = 0.1, maxRequestTime = 3000 } = {}
) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Math.random() < errorRate) {
        const newError = new Error()
        const errors = [newError, ...customErrors]
        if (customErrors.length) {
          alert(errors[1].message)
        } else {
          alert('Что-то пошло не так')
        }
        // const error = errors[Math.floor(Math.random() * error.length)]
        reject(errors)
      } else {
        resolve(data)
      }
    }, Math.random() * maxRequestTime)
  })

const mockedGameApi = {
  getPlayersInfo: () => mockApiCall(mockPlayers),
  makeMove: (userId, cell) => {
    console.log('makeMove')
    return mockApiCall(undefined).then(() => game.makeMove(cell))
  },

  moveMade: (userId, cell) => {
    console.log('moveMade')
    return mockApiCall(game.cell)
  },

  moveFailed: () => {
    console.log('moveFailed')
    return mockApiCall(undefined, {
      errorRate: 1,
      customErrors: [new Error('Клетка занята')],
    })
  },

  sendMessage: (text) => {
    console.log('sendMessage')
    return mockApiCall(undefined).then(() => game.sendMessage(text))
  },
}

export default mockedGameApi
