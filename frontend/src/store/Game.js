import { makeAutoObservable, observable, action, computed } from 'mobx'

class Game {
  gameId = null
  mode = 'X'
  field = [null, null, null, null, null, null, null, null, null]
  players = []
  roles = {}
  messages = []

  constructor() {
    makeAutoObservable(this, {
      gameId: observable,
      mode: observable,
      field: observable,
      players: observable,
      roles: observable,
      messages: observable,

      newGame: action,
      setPlayers: action,
      setRoles: action,
      setField: action,
      setMessages: action,

      winner: computed,
      isTie: computed,
      isOver: computed,
    })
  }

  moveMade(i, symbol) {
    this.field[i] = symbol
    this.mode = symbol === 'X' ? 'O' : 'X'
  }

  get winner() {
    if (this.isWinner('X')) {
      return 'X'
    }
    if (this.isWinner('O')) {
      return 'O'
    }
    return null
  }

  get isTie() {
    return !this.winner && !this.field.includes(null)
  }

  get isOver() {
    return this.winner || !this.field.includes(null)
  }

  isWinner(symbol) {
    const combs = [
      ['0', '1', '2'],
      ['3', '4', '5'],
      ['6', '7', '8'],
      ['0', '3', '6'],
      ['1', '4', '7'],
      ['2', '5', '8'],
      ['0', '4', '8'],
      ['2', '4', '6'],
    ]

    for (let comb of combs) {
      if (
        this.field[comb[0]] !== null &&
        this.field[comb[0]] === this.field[comb[1]] &&
        this.field[comb[1]] === this.field[comb[2]]
      ) {
        return this.field[comb[0]] === symbol
      }
    }
  }

  newGame(id) {
    this.gameId = id
    this.mode = 'X'
    this.field = [null, null, null, null, null, null, null, null, null]
    this.messages = []
  }

  setField(data) {
    this.field = data
  }
  setPlayers(data) {
    this.players = data
  }

  setRoles(data) {
    this.roles = data
  }

  setMessages(data) {
    this.messages = data
  }
}

const game = new Game()

export default game
