export const formatDate = (date) => {
  const month = date.slice(6, 7)
  const day = date.slice(8, 10)
  const year = date.slice(0, 4)

  const months = {
    1: 'января',
    2: 'февраля',
    3: 'марта',
    4: 'апреля',
    5: 'мая',
    6: 'июня',
    7: 'июля',
    8: 'августа',
    9: 'сентября',
    10: 'октября',
    11: 'ноября',
    12: 'декабря',
  }
  return (date = `${day} ${months[month]} ${year}`)
}
export const formatUserName = (surName, firstName, secondName) => {
  return `${surName} ${firstName[0]}.${secondName[0]}.`
}

export const formatTime = (time) => {
  return (
    new Date(time).getHours() +
    ':' +
    String(new Date(time).getMinutes()).padStart(2, '0')
  )
}
