import { useEffect, useState, useContext } from 'react'
import AuthContext from '../context/AuthContext'

export const useAuthStatus = () => {
  const [isAuthorize, setIsAuthorize] = useState(false)
  const [isLoading, setIsLoading] = useState(true)

  const user = useContext(AuthContext)

  useEffect(() => {
    if (user) {
      setIsAuthorize(true)
    } else {
      setIsAuthorize(false)
    }
    setIsLoading(false)
  }, [user])

  return { isAuthorize, isLoading }
}
