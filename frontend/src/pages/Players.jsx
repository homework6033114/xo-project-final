import React, { useEffect, useState, useContext } from 'react'
import Header from '../components/Header/Header'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import StatusM from '../components/UI/Status/StatusM'
import Button from '../components/UI/Button/Button'
import SwitchToggle from '../components/UI/SwitchToggle/SwitchToggle'
import AuthContext from '../context/AuthContext'
import usersService from '../API/UsersService'
import socket from '../sockets/socket'

const Players = () => {
  const { user: currentUser } = useContext(AuthContext)
  const [players, setPlayers] = useState([])
  const [isChecked, setIsChecked] = useState(false)

  useEffect(() => {
    const getPlayers = async () => {
      const users = await usersService.getPlayers()
      if (users) {
        if (!isChecked) {
          setPlayers(
            users.filter(
              (user) => user.statusGame !== null && user.id !== currentUser.id
            )
          )
        } else {
          setPlayers(
            users.filter(
              (user) => user.statusGame && user.id !== currentUser.id
            )
          )
        }
      }
    }
    getPlayers()
  }, [currentUser, isChecked, players])

  const onInvite = (userId) => {
    socket.invite(currentUser, userId)
  }

  const onChange = () => {
    setIsChecked(!isChecked)
  }

  return (
    <>
      <Header />
      <TableContainer className="activePlayers">
        <div className="header">
          <Title>Активные игроки</Title>
          <div className="switchToggle">
            <p>Только свободные</p>
            <SwitchToggle onChange={onChange} isChecked={isChecked} />
          </div>
        </div>

        <div className="table-activePlayers scrollable">
          {players.map((player) => (
            <div className="row" key={player.id}>
              <div className="player">{` ${player.surName} ${player.firstName} ${player.secondName}`}</div>
              <div className="status">
                {player.statusGame && (
                  <StatusM status="available">Свободен</StatusM>
                )}
                {!player.statusGame && (
                  <StatusM status="playing">В игре</StatusM>
                )}
              </div>
              <div className="btn">
                {player.statusGame && (
                  <Button version="primary" onClick={() => onInvite(player.id)}>
                    Позвать играть
                  </Button>
                )}
                {!player.statusGame && (
                  <Button version="secondary" isDisabled={true}>
                    Позвать играть
                  </Button>
                )}
              </div>
            </div>
          ))}
        </div>
      </TableContainer>
    </>
  )
}

export default Players
