import React, { useEffect, useState } from 'react'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import Header from '../components/Header/Header'
import usersService from '../API/UsersService'

const Rating = () => {
  const [users, setUsers] = useState([])

  useEffect(() => {
    const getData = async () => {
      const data = await usersService.getRating()
      if (data) {
        setUsers(data)
      }
    }
    getData()
  }, [])

  return (
    <>
      <Header />
      <TableContainer className="rating">
        <Title>Рейтинг игроков</Title>

        <div className="table-rating">
          <div className="row">
            <div className="userName table-headers">ФИО</div>
            <div className="totalGames table-headers">Всего игр</div>
            <div className="wins table-headers">Победы</div>
            <div className="losses table-headers">Проигрыши</div>
            <div className="winRate table-headers">Процент побед</div>
          </div>
          <div className="scrollable">
            {users.map((user) => (
              <div className="row" key={user.userId}>
                <div className="userName">{`${user.surName} ${user.firstName} ${user.secondName}`}</div>
                <div className="totalGames">{user.games_cnt}</div>
                <div className="wins success">{user.wins_cnt}</div>
                <div className="losses danger">{user.loses_cnt}</div>
                <div className="winRate">
                  {Math.round(`${user.wins_perc}`)}%
                </div>
              </div>
            ))}
          </div>
        </div>
      </TableContainer>
    </>
  )
}

export default Rating
