import React, { useContext, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Input from '../components/UI/Input/Input'
import Button from '../components/UI/Button/Button'
import Title from '../components/UI/Text/Title'
import AuthContext from '../context/AuthContext'
import authService from '../API/AuthService'
import { toast } from 'react-toastify'

const Login = () => {
  const { setUser } = useContext(AuthContext)
  const navigate = useNavigate()

  const [formdata, setFormData] = useState({
    login: '',
    password: '',
  })
  const { login, password } = formdata

  const [loginIsValid, setLoginIsValid] = useState(true)
  const [passwordIsValid, setPasswordIsValid] = useState(true)

  const pasteHandler = (e) => {
    e.preventDefault()
  }

  const copyHandler = (e) => {
    e.preventDefault()
  }

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value.replace(/[^\w.]/g, ''),
    }))
  }

  const onSubmit = async (e) => {
    e.preventDefault()

    const userData = {
      login,
      password,
    }

    try {
      const data = await authService.authorize(userData)
      if (data) {
        localStorage.setItem('user', JSON.stringify(data))
        setUser(data)
        navigate('/users')
        setFormData({
          login: '',
          password: '',
        })
        setLoginIsValid(true)
        setPasswordIsValid(true)
      }
    } catch (err) {
      const errorMessage = err.response.data.message
      if (errorMessage === 'Логин неверный') {
        setLoginIsValid(false)
      } else if (errorMessage === 'Пароль неверный') {
        setPasswordIsValid(false)
      } else {
        toast.error(errorMessage)
      }
    }
  }

  return (
    <>
      <div className="center">
        <div className="login-container">
          <div className="img">
            <img src="../imgs/dog.svg" alt="dog" />
          </div>
          <Title>Войдите в игру</Title>
          <form className="login-form" onSubmit={onSubmit}>
            <div className="inputs-group">
              <div className="input-control">
                <Input
                  type="text"
                  name="login"
                  value={login}
                  isValid={loginIsValid}
                  placeholder="Логин"
                  message="Неверный логин"
                  onPaste={pasteHandler}
                  onCopy={copyHandler}
                  onChange={onChange}
                />
                {!loginIsValid && (
                  <p className="error-message">Неверный логин</p>
                )}
              </div>
              <div className="input-control">
                <Input
                  isValid={passwordIsValid}
                  value={password}
                  name="password"
                  type="password"
                  placeholder="Пароль"
                  message="Неверный пароль"
                  onPaste={pasteHandler}
                  onCopy={copyHandler}
                  onChange={onChange}
                />
                {!passwordIsValid && (
                  <p className="error-message">Неверный пароль</p>
                )}
              </div>
            </div>
            <Button
              type="submit"
              isDisabled={!login || !password}
              version="primary"
            >
              Войти
            </Button>
          </form>
        </div>
      </div>
    </>
  )
}

export default Login
