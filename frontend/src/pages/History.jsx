import React, { useState, useEffect } from 'react'
import Header from '../components/Header/Header'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import WinnerIcon from '../components/UI/Icons/WinnerIcon'
import SymbolSmall from '../components/UI/Symbol/SymbolSmall'
import { formatDate, formatUserName } from '../utils/formatValue'
import gameService from '../API/GameService'

const History = () => {
  const [games, setGames] = useState([])

  const calcGameTime = (start, finish) => {
    const time = new Date(finish).getTime() - new Date(start).getTime()
    const minutes = Math.round(time / 60000)
    const seconds = ((time % 60000) / 1000).toFixed(0)
    return `${minutes} мин. ${seconds < 10 ? '0' : ''}${seconds} сек`
  }
  useEffect(() => {
    const getData = async () => {
      const data = await gameService.getGames()
      if (data) {
        setGames(data)
      }
    }
    getData()
  }, [])

  return (
    <>
      <Header />
      <TableContainer className="history">
        <Title>История игр</Title>
        <div className="table-history">
          <div className="row">
            <div className="players table-headers">Игроки</div>
            <div className="date table-headers">Дата</div>
            <div className="time table-headers">Время игры</div>
          </div>
          <div className="scrollable">
            {games.map((game) => (
              <div className="row" key={game.id}>
                <div className="players">
                  <div className="player">
                    <div className="symbol">
                      <SymbolSmall symbol="X" />
                    </div>
                    <div>
                      {formatUserName(
                        game.surName1,
                        game.firstName1,
                        game.secondName1
                      )}
                    </div>

                    {game.gameResult && <WinnerIcon />}
                  </div>
                  <div className="text-bold">против</div>
                  <div className="player">
                    <div className="symbol">
                      <SymbolSmall symbol="O" />
                    </div>
                    <div>
                      {formatUserName(
                        game.surName2,
                        game.firstName2,
                        game.secondName2
                      )}
                    </div>
                    {!game.gameResult && <WinnerIcon />}
                  </div>
                </div>
                <div className="date">{formatDate(game.gameBegin)}</div>
                <div className="time">
                  {calcGameTime(game.gameBegin, game.gameEnd)}
                </div>
              </div>
            ))}
          </div>
        </div>
      </TableContainer>
    </>
  )
}

export default History
