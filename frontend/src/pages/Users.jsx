import React, { useState, useEffect } from 'react'
import Header from '../components/Header/Header'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import Button from '../components/UI/Button/Button'
import GenderIcon from '../components/UI/Icons/GenderIcon'
import StatusM from '../components/UI/Status/StatusM'
import BlockIcon from '../components/UI/Icons/BlockIcon'
import AddUserModal from '../components/Modals/AddUser'
import { formatDate } from '../utils/formatValue'
import usersService from '../API/UsersService'

const Users = () => {
  const [users, setUsers] = useState([])
  const [modalIsOpen, setModalIsOpen] = useState(false)

  useEffect(() => {
    const getUsers = async () => {
      const data = await usersService.getUsers()
      if (data) {
        setUsers(data)
      }
    }
    getUsers()
  }, [users])

  const onOpenModal = () => {
    setModalIsOpen(true)
  }
  const onCloseModal = () => {
    setModalIsOpen(false)
  }

  const addUser = async ({ userName, age, gender }) => {
    userName = userName.split(' ')
    gender = gender === 'M' ? true : false
    const newUser = {
      surName: userName[0],
      firstName: userName[1],
      secondName: userName[2],
      age,
      gender,
      statusActive: true,
      isAdmin: false,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
    }
    await usersService.addUser(newUser)
  }

  const updateStatus = async (id, status) => {
    const updatedStatus = {
      statusActive: !status,
      updatedAt: new Date().toISOString(),
    }
    await usersService.updateUser(id, updatedStatus)
  }

  return (
    <>
      <Header />
      <TableContainer className="users">
        <div className="header">
          <Title>Список игроков</Title>
          <div className="btn">
            <Button version="primary" onClick={onOpenModal}>
              Добавить игрока
            </Button>
          </div>
        </div>

        <div className="table-users">
          <div className="row ">
            <div className="user table-headers">ФИО</div>
            <div className="age table-headers">Возраст</div>
            <div className="sex table-headers">Пол</div>
            <div className="status table-headers">Статус</div>
            <div className="date table-headers">Создан</div>
            <div className="date table-headers">Изменен</div>
            <div className="btn"></div>
          </div>
          <div className="scrollable">
            {users.map((user) => (
              <div className="row " key={user.id}>
                <div className="user">{`${user.surName} ${user.firstName} ${user.secondName}`}</div>
                <div className="age">{user.age}</div>
                <div className="sex">
                  <GenderIcon sex={user.gender} />
                </div>
                <div className="status">
                  {user.statusActive && (
                    <StatusM status="available">Активен</StatusM>
                  )}
                  {!user.statusActive && (
                    <StatusM status="blocked">Заблокирован</StatusM>
                  )}
                </div>
                <div className="date">{formatDate(user.createdAt)}</div>
                <div className="date">{formatDate(user.updatedAt)}</div>
                <div className="btn">
                  {user.statusActive ? (
                    <Button
                      version="secondary"
                      onClick={() => updateStatus(user.id, user.statusActive)}
                    >
                      Заблокировать
                    </Button>
                  ) : (
                    <Button
                      version="secondary"
                      onClick={() => updateStatus(user.id, user.statusActive)}
                    >
                      <BlockIcon />
                      Разблокировать
                    </Button>
                  )}
                </div>
              </div>
            ))}
          </div>
          {/* {usersData.map((user) => (
            <div className="row " key={user.id}>
              <div className="user">{user.userName}</div>
              <div className="age">{user.age}</div>
              <div className="sex">
                <GenderIcon sex={user.sex} />
              </div>
              <div className="status">
                {user.userStatus === 'Свободен' && (
                  <StatusM status="available">Активен</StatusM>
                )}
                {user.userStatus === 'Заблокирован' && (
                  <StatusM status="blocked">Заблокирован</StatusM>
                )}
              </div>
              <div className="date">{user.createdAt}</div>
              <div className="date">{user.updatedAt}</div>
              <div className="btn">
                {user.userStatus === 'Свободен' ? (
                  <Button
                    version="secondary"
                    onClick={() => updateStatus(user.id)}
                  >
                    Заблокировать
                  </Button>
                ) : (
                  <Button
                    version="secondary"
                    onClick={() => updateStatus(user.id)}
                  >
                    <BlockIcon />
                    Разблокировать
                  </Button>
                )}
              </div>
            </div>
          ))} */}

          {/* {usersData.map((user) => (
            <div className="row " key={user.id}>
              <div className="user">{user.userName}</div>
              <div className="age">{user.age}</div>
              <div className="sex">
                <GenderIcon sex={user.sex} />
              </div>
              <div className="status">
                {user.userStatus === 'Свободен' ? (
                  <StatusM status="available">Активен</StatusM>
                ) : (
                  <StatusM status="blocked">{user.userStatus}</StatusM>
                )}
              </div>
              <div className="date">{user.createdAt}</div>
              <div className="date">{user.updatedAt}</div>
              <div className="btn">
                {user.userStatus === 'Свободен' ? (
                  <Button version="secondary" onClick={toggleStatus}>
                    Заблокировать{' '}
                  </Button>
                ) : (
                  <Button version="secondary" onClick={toggleStatus}>
                    <BlockIcon />
                    Разблокировать
                  </Button>
                )}
              </div>
            </div>
          ))} */}
        </div>
      </TableContainer>
      <AddUserModal
        onClose={onCloseModal}
        modalIsOpen={modalIsOpen}
        addUser={addUser}
        setModalIsOpen={setModalIsOpen}
      />
    </>
  )
}

export default Users
