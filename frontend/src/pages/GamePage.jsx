import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Header from '../components/Header/Header'
import PlayersList from '../components/PlayersList/PlayersList'
import GameBoard from '../components/Game/GameBoard'
import Chat from '../components/Chat/Chat'
import GameContext from '../context/GameContext'
import Spinner from '../components/UI/Spinner/Spinner'
import gameService from '../API/GameService'
import usersService from '../API/UsersService'

const Game = () => {
  const game = useContext(GameContext)
  const { gameId } = useParams()
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    const getGameInfo = async (id) => {
      const playersInfo = await gameService.getGame(id)
      const winRatePlayer1 = await usersService.getPlayerRating(
        playersInfo.playerId1
      )
      const winRatePlayer2 = await usersService.getPlayerRating(
        playersInfo.playerId2
      )

      const players = [
        {
          id: playersInfo.playerId1,
          surName: playersInfo.surName1,
          firstName: playersInfo.firstName1,
          secondName: playersInfo.secondName1,
          userName: `${playersInfo.surName1} ${playersInfo.firstName1} ${playersInfo.secondName1}`,
          winRate: winRatePlayer1,
        },
        {
          id: playersInfo.playerId2,
          surName: playersInfo.surName2,
          firstName: playersInfo.firstName2,
          secondName: playersInfo.secondName2,
          userName: `${playersInfo.surName2} ${playersInfo.firstName2} ${playersInfo.secondName2}`,
          winRate: winRatePlayer2,
        },
      ]

      game.setPlayers(players)
      game.setRoles({
        X: players[0].id,
        O: players[1].id,
      })
      game.newGame(id)
      setIsLoading(false)
    }

    getGameInfo(gameId)
  }, [game, game.field, gameId])

  if (isLoading) {
    return <Spinner />
  }

  return (
    <>
      <Header />

      <main>
        <div className="main-container">
          <PlayersList />
          <GameBoard />
          <Chat />
        </div>
      </main>
    </>
  )
}

export default Game
