SELECT user_id, users."surName", users."firstName", users."secondName",
	SUM(wins_cnt + loses_cnt + draws_cnt) AS games_cnt,
	SUM(wins_cnt) AS wins_cnt,
	SUM(loses_cnt) AS loses_cnt,
	SUM(wins_cnt::float) / SUM(wins_cnt + loses_cnt + draws_cnt)*100 AS wins_perc,
	SUM(wins_cnt * (CASE WHEN symbol = 'X' THEN 0.9 ELSE 1 END) -
   loses_cnt * (CASE WHEN symbol = 'X' THEN 1.1 ELSE 1 END) + draws_cnt * 0.25) /
   (COUNT(DISTINCT period)) AS rating
FROM counters
JOIN users ON counters.user_id = users.id
WHERE period >= date_trunc('month', now() - interval '6 month')
AND users."statusActive" = true 
GROUP BY user_id, users."surName", users."firstName", users."secondName"
HAVING SUM(wins_cnt + loses_cnt + draws_cnt) > 50
ORDER BY rating DESC;