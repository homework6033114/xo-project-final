INSERT INTO counters (user_id, symbol, period, wins_cnt, loses_cnt, draws_cnt)
SELECT u.id AS user_id,
		'X' AS symbol,
		date_trunc('month', g."gameBegin") AS period,
		COUNT(g.id) FILTER (WHERE g."gameResult" = TRUE) AS wins_cnt,
		COUNT(g.id) FILTER (WHERE g."gameResult" = FALSE) AS loses_cnt,
		COUNT(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws_cnt
	FROM users AS u
		JOIN games AS g ON u.id = g."idPlayer1"
	GROUP BY u.id, date_trunc('month', g."gameBegin")
	UNION ALL
	SELECT u.id AS user_id,
		'O' AS symbol,
		date_trunc('month', g."gameBegin") AS period,
		COUNT(g.id) FILTER (WHERE g."gameResult" = FALSE) AS wins_cnt,
		COUNT(g.id) FILTER (WHERE g."gameResult" = TRUE) AS loses_cnt,
		COUNT(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws_cnt
	FROM users AS u
			JOIN games AS g ON u.id = g."idPlayer2"
	GROUP BY u.id, date_trunc('month', g."gameBegin")
	