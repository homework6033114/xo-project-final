SELECT 
EXTRACT(YEAR FROM period) AS year,
EXTRACT(MONTH FROM period) AS month,
symbol,
SUM(wins_cnt) AS wins_cnt,
SUM(loses_cnt) AS loses_cnt,
SUM(draws_cnt) AS draws_cnt,
SUM(wins_cnt::float) / SUM(wins_cnt + loses_cnt + draws_cnt)*100 AS wins_perc,
SUM(wins_cnt + loses_cnt + draws_cnt) AS games_cnt,
SUM(wins_cnt * (CASE WHEN symbol = 'x' THEN 0.9 ELSE 1 END) -
   loses_cnt * (CASE WHEN symbol = 'x' THEN 1.1 ELSE 1 END) + draws_cnt * 0.25) AS rating
   
FROM counters			 
WHERE user_id = 200	

GROUP BY year, CUBE (month,
symbol)
ORDER BY year, month, symbol 			
				


 
