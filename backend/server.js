const app = require('./app')
const http = require('http')
const { Server } = require('socket.io')
const knexConfig = require('./db/knexfile')
const knex = require('knex')(knexConfig[process.env.NODE_ENV])

const donenv = require('dotenv').config()
const port = process.env.PORT || 5000
app.set('port', port)
const server = http.createServer(app)

const io = new Server(server, {
  cors: {
    origin: 'http://localhost:3000',
  },
})

io.on('connection', async (socket) => {
  socket.on('players:join', ({ user }, cb) => {
    socket.join(user.id)
    io.to(user.id).emit('message', `${user.userName} онлайн`)
  })

  socket.on('players:invite', ({ fromPlayer, toPlayer, message }) => {
    console.log(fromPlayer, toPlayer)
    io.to(toPlayer).emit('players:invited', {
      fromPlayer: toPlayer,
      toPlayer: fromPlayer,
      message,
    })
  })

  socket.on('players:accept', ({ fromPlayer, toPlayer, message }) => {
    io.to(toPlayer).emit('players:accepted', { fromPlayer, toPlayer, message })
  })

  socket.on('players:decline', ({ toPlayer, message }) => {
    io.to(toPlayer).emit('players:declined', { message })
  })

  socket.on('game:join', ({ toPlayer, gameId }) => {
    io.to(toPlayer).emit('game:joined', { gameId })
  })
  socket.on('game:start', ({ room }, cb) => {
    socket.join(room), cb(room)
  })

  socket.on('game:make-move', async ({ gameId, position, symbol }) => {
    try {
      await knex('moves').insert({ game_id: gameId, position, symbol })
    } catch (err) {
      console.log(err)
    }
    io.to(`${gameId}`).emit('game:move-made', { position, symbol })
  })

  socket.on('game:message', async ({ message }) => {
    await knex('messenger').insert(message)
  })
})

server.listen(port, () => console.log(`server listening on ${port}`))
