const express = require('express')
const cookieParser = require('cookie-parser')
const path = require('path')
const donenv = require('dotenv').config()
const knexConfig = require('./db/knexfile')
const authRoute = require('./routes/auth')
const usersRouter = require('./routes/users')
const gamesRouter = require('./routes/games')
const gameRouter = require('./routes/game')
const { errorHandler } = require('./middleware/errorMiddleware')

const app = express()

const knex = require('knex')(knexConfig[process.env.NODE_ENV])

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/api/auth', authRoute)
app.use('/api/users', usersRouter)
app.use('/api/games', gamesRouter)

app.use(errorHandler)

module.exports = app
