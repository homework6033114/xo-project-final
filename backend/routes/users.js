const express = require('express')
const router = express.Router()
const usersController = require('../controllers/usersController')
const { protect, admin } = require('../middleware/authMiddleware')

router
  .route('/')
  .get(protect, admin, usersController.getUsers)
  .post(protect, admin, usersController.addUser)

router.route('/:userId').put(protect, admin, usersController.updateUser)

router.route('/active').get(protect, usersController.getActivePlayers)
router.route('/rating').get(protect, usersController.getRating)
router.route('/rating/:userId').get(protect, usersController.getRatingById)

module.exports = router
