//Homework-11 only
const express = require('express')
const {
  createField,
  getFieldByIdx,
  updateFieldByIdx,
  deleteFieldByIdx,
} = require('../controllers/gameController')

const router = express.Router()

router
  .route('/')
  .post(createField)
  .get(getFieldByIdx)
  .put(updateFieldByIdx)
  .delete(deleteFieldByIdx)

module.exports = router
