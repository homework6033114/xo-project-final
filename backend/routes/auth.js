const express = require('express')
const authController = require('../controllers/authController')
const { protect, admin } = require('../middleware/authMiddleware')

const router = express.Router()

router.post('/', authController.authUser)
router.post('/logout', protect, authController.logoutUser)
router.post('/register/:userId', protect, admin, authController.registerUser)

module.exports = router
