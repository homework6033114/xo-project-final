const express = require('express')
const gamesController = require('../controllers/gamesController')

const chatController = require('../controllers/chatController')

const router = express.Router()

const { protect } = require('../middleware/authMiddleware')

router
  .route('/')
  .get(protect, gamesController.getGames)
  .post(protect, gamesController.createGame)

router
  .route('/:gameId')
  .get(protect, gamesController.getGame)
  .put(protect, gamesController.updateGame)

router
  .route('/:gameId/chat')
  .get(protect, chatController.getMessages)
  .post(protect, chatController.addMessage)

router.route('/:gameId/moves').get(protect, gamesController.getMoves)

module.exports = router
