const asyncHandler = require('express-async-handler')
const jwt = require('jsonwebtoken')
const knexConfig = require('../db/knexfile')
const knex = require('knex')(knexConfig[process.env.NODE_ENV])

const protect = asyncHandler(async (req, res, next) => {
  let token
  token = req.cookies.jwt
  if (token) {
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET)

      // const user = await knex('accounts').where('idUser', decoded.id)

      const user = await knex('users').where('id', decoded.id)

      req.user = user[0]

      next()
    } catch (error) {
      console.error(error)
      res
        .status(401)
        // throw new Error('Not authorized, token failed')
        .json({ message: 'Not authorized, token failed' })
    }
  } else {
    res
      .status(401)
      // throw new Error('Not authorized, no token')
      .json({ message: 'Not authorized, no token' })
  }
})

const admin = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next()
  } else {
    res
      .status(401)
      // throw new Error('Not authorized as an admin')
      .json({ message: 'Not authorized as an admin' })
  }
}
module.exports = { protect, admin }
