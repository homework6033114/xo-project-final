const knex = require('knex')

exports.up = function (knex) {
  return knex.schema.createTable('games', function (table) {
    table.increments('id').primary().notNullable()
    table.integer('idPlayer1').unsigned().notNullable()
    table
      .foreign('idPlayer1')
      .references('id')
      .inTable('users')
      .onDelete('Cascade')
      .onUpdate('Cascade')
    table.integer('idPlayer2').unsigned().notNullable()
    table
      .foreign('idPlayer2')
      .references('id')
      .inTable('users')
      .onDelete('Cascade')
      .onUpdate('Cascade')
    table.boolean('gameResult')
    table.timestamp('gameBegin', { useTz: false }).notNullable()
    table.timestamp('gameEnd', { useTz: false })
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('games')
}
