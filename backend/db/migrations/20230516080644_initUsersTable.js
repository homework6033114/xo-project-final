const knex = require('knex')

exports.up = function (knex) {
  return knex.schema.createTable('users', function (table) {
    table.increments('id').primary()
    table.string('surName', 255).notNullable()
    table.string('firstName', 255).notNullable()
    table.string('secondName', 255).notNullable()
    table.integer('age').notNullable()
    table.boolean('gender').notNullable()
    table.boolean('statusActive').notNullable()
    table.boolean('statusGame').defaultTo(null)
    table.boolean('isAdmin').notNullable()
    table.timestamp('createdAt', { useTz: false }).defaultTo(knex.fn.now())
    table.timestamp('updatedAt', { useTz: false }).defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('users')
}
