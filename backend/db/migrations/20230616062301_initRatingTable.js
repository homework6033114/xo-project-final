exports.up = async function (knex) {
  await knex.schema.createTable('counters', function (table) {
    table.increments('id').primary()
    table.integer('user_id').notNullable()
    table.foreign('user_id').references('id').inTable('users')
    table.string('symbol', 1).notNullable()
    table
      .timestamp('period')
      .notNullable()
      .defaultTo(knex.raw(`date_trunc('month', now())`))
    table.integer('wins_cnt').notNullable().defaultTo(0)
    table.integer('loses_cnt').notNullable().defaultTo(0)
    table.integer('draws_cnt').notNullable().defaultTo(0)
    table.unique(['user_id', 'symbol', 'period'])
  })
}
exports.down = function (knex) {
  return knex.schema.dropTable('counters')
}
