exports.up = async function (knex) {
  await knex.schema.createTable('moves', (table) => {
    table.increments('id').primary()
    table.integer('game_id').notNullable().references('id').inTable('games')
    table.integer('position').notNullable()
    table.string('symbol', 1).notNullable()
    table.unique(['game_id', 'position'])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('moves')
}
