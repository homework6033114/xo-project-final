const donenv = require('dotenv').config()

const password = (module.exports = {
  development: {
    client: 'pg',

    connection: {
      host: '127.0.0.1',
      port: 5433,
      user: 'postgres',
      // password: process.env.PG_PASS,
      password: 'Element5',
      database: 'db_xoxo_dev',
    },

    migrations: {
      directory: 'migrations',
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: 'seeds',
    },
  },

  // production: {
  //   client: 'pg',
  //   connection: {
  //     host: '127.0.0.1',
  //     port: 5433,
  //     user: 'postgres',
  //     password: process.env.PG_PASS,
  //     database: 'db_xoxo_home',
  //   },

  //   searchPath: ['knex', 'public'],
  //   migrations: {
  //     tableName: 'knex_migrations',
  //   },
  // },
})
