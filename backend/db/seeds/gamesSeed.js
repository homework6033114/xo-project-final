const { faker } = require('@faker-js/faker')

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

const generateGames = (number, minDate, maxDate, minId, maxId) => {
  let games = []
  for (let i = 0; i < number; i++) {
    let gameBegin = faker.date.between({ from: minDate, to: maxDate })
    let minDateEnd = new Date(gameBegin.getTime() + 1000 * 60 * 30)
    let maxDateEnd = new Date(gameBegin.getTime() + 1000 * 60 * 30)
    let gameEnd = faker.date.between({ from: minDateEnd, to: maxDateEnd })

    games.push({
      idPlayer1: faker.number.int({ min: minId, max: maxId }),
      idPlayer2: faker.number.int({ min: minId + 1, max: maxId + 1 }),
      gameResult: faker.helpers.arrayElement([true, false, null]),
      gameBegin,
      gameEnd,
    })
  }
  return games
}

exports.seed = async function (knex) {
  // у 10% пользователей игры должны быть только с мая 2022 по ноябрь 2022
  for (let j = 0; j < 10; j++) {
    const games = generateGames(
      10000,
      '2022-05-01T00:00:00.000Z',
      '2022-10-31T00:00:00.000Z',
      30,
      219
    )
    await knex('games').insert(games)
  }
  // У 10% пользователей должен быть один ярко выраженный по играм месяц с декабря 2022 по май 2023
  for (let j = 0; j < 10; j++) {
    const games = generateGames(
      10000,
      '2023-04-01T00:00:00.000Z',
      '2023-04-30T00:00:00.000Z',
      220,
      429
    )
    await knex('games').insert(games)
  }
  // У 10% пользователей должен быть месяц с пропуском по играм с декабря 2022 по май 2023
  for (let j = 0; j < 5; j++) {
    const games = generateGames(
      10000,
      '2022-05-01T00:00:00.000Z',
      '2022-12-31T00:00:00.000Z',
      430,
      629
    )
    await knex('games').insert(games)
  }
  for (let j = 0; j < 5; j++) {
    const games = generateGames(
      10000,
      '2023-02-01T00:00:00.000Z',
      '2023-05-31T00:00:00.000Z',
      430,
      629
    )
    await knex('games').insert(games)
  }
  // У остальных  пользователей должны быть игры
  for (let j = 0; j < 70; j++) {
    const games = generateGames(
      10000,
      // '2022-05-01T00:00:00.000Z',
      '2023-02-01T00:00:00.000Z',
      '2023-05-31T00:00:00.000Z',
      630,
      2029
    )
    await knex('games').insert(games)
  }
}
