const { faker } = require('@faker-js/faker')
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  const users = []

  for (let i = 0; i < 2000; i++) {
    users.push({
      surName: faker.person.lastName(),
      firstName: faker.person.firstName(),
      secondName: faker.person.middleName(),
      age: faker.number.int({ min: 6, max: 100 }),
      gender: faker.datatype.boolean(),
      statusActive: faker.datatype.boolean(),
      statusGame: faker.datatype.boolean(),
      isAdmin: false,
      createdAt: faker.date.between({
        from: '2022-05-01T00:00:00.000Z',
        to: '2022-12-31T00:00:00.000Z',
      }),
      updatedAt: faker.date.between({
        from: '2022-06-01T00:00:00.000Z',
        to: '2023-05-31T00:00:00.000Z',
      }),
    })
  }
  // Deletes ALL existing entries
  // await knex('users').del()
  await knex('users').insert(users)
}
