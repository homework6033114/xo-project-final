const knex = require('knex')

exports.seed = async function (knex) {
  await knex('accounts').truncate()
  await knex('accounts').insert([
    {
      idUser: 1,
      login: 'Andreeva',
      password: '$2a$10$rMUI3nDLM3raX5sLh/JH0eq2smg8MvG1EFKzsyHM1WfNdMSo6.DEK',
    },
    {
      idUser: 2,
      login: 'Aleksandrov',
      password: '$2a$10$o82MzKzCtgx2BThJEu4XVeHoLh7zYvxAup3LGDdVwsvvRVQVp7J9y',
    },
    {
      idUser: 3,
      login: 'Shevchenko',
      password: '$2a$10$wy3.E3FuWK0W63ZFIbCXvu0LZaui2jXRVWajKmFKcBfva0IuS27O6',
    },
    {
      idUser: 4,
      login: 'Mazajlo',
      password: '$2a$10$rWRdcfuFPqWN59mWgGH.f.xEPipMDu3.kNZijDsjeYxA8ho/azUiS',
    },
    {
      idUser: 5,
      login: 'Loginov',
      password: '$2a$10$5xbft9k.spspgga/gyl6Qu1ShAWKiOEh2/RLJdwXvdJynvN6v44HS',
    },
    {
      idUser: 6,
      login: 'Borisov',
      password: '$2a$10$bHmixxGtYECEuJbCFL0FEOpWea/WFm9TiMRU1Ll2Rm3rUHaIkSPQ6',
    },
    {
      idUser: 7,
      login: 'Solovyov',
      password: '$2a$10$ihNY2lHhdCRQXXeG3mu4B.PE3ldm1Earaxc5Wb82lrDcZ19OcVzpq',
    },
    {
      idUser: 8,
      login: 'Negoda',
      password: '$2a$10$SSH2fvhXfmWZs1HicK13OufabD3CvNofztU3XlYehS4Ax.XLvyiUi',
    },
    {
      idUser: 9,
      login: 'Gordeev',
      password: '$2a$10$sSBgAzrFFxrHyXmKhZmRhe2uXaDu7sDo4A/7787VaavbICT8YnJFi',
    },
    {
      idUser: 10,
      login: 'Mnogogreshnyj',
      password: '$2a$10$lLo6.aIYgg8DNYINiwAQ5.YHNuevNX33FlQrr.pNvPPxKMntnx1bi',
    },
    {
      idUser: 11,
      login: 'Volkov',
      password: '$2a$10$ue8f0mYV5uUeZAv3GUIXE.r.IM0C4dE3rvVGFghy60pl7w77a7Kkq',
    },
  ])
}
