const asyncHandler = require('express-async-handler')
const bcrypt = require('bcryptjs')
const knexConfig = require('../db/knexfile')
const knex = require('knex')(knexConfig[process.env.NODE_ENV])

const generateToken = require('../utils/generateToken')

// @desc   Авторизация пользователя
// @route  /api/auth/
// @access Public
exports.authUser = asyncHandler(async (req, res, next) => {
  const { login, password } = req.body

  const data = await knex('accounts')
    .where('login', login)
    .innerJoin('users', 'accounts.idUser', 'users.id')
    .select(
      'accounts.idUser',
      'accounts.password',
      'users.isAdmin',
      'users.surName',
      'users.firstName',
      'users.secondName',
      'users.statusActive'
    )

  const user = data[0]

  if (user) {
    if (!user.statusActive) {
      res.status(403)
      throw new Error('Аккаунт заблокирован')
    }
    if (await bcrypt.compare(password, user.password)) {
      generateToken(res, user.idUser),
        res.status(200).json({
          id: user.idUser,
          userName: `${user.surName} ${user.firstName} ${user.secondName}`,
          isAdmin: user.isAdmin,
        })
    } else {
      res.status(401)
      throw new Error('Пароль неверный')
    }
  } else {
    res.status(401)
    throw new Error('Логин неверный')
  }
})

// @desc    Logout user
// @route   POST /api/auth/logout
// @access  Private
exports.logoutUser = (req, res) => {
  res.cookie('jwt', '', {
    httpOnly: true,
    expires: new Date(0),
  })
  res.status(200).json({ message: 'Вы успешно вышли из аккаунта' })
}

// @desc  Регистрация нового пользователя
// @route  /api/auth/register
// @access Private (admin)
exports.registerUser = asyncHandler(async (req, res) => {
  const { userId } = req.params
  const { login, password } = req.body

  if (password.length < 6) {
    return res.status(400).json({ message: 'Пароль меньше 6 символов' })
  }

  if (!login || !password) {
    res.status(405)
    throw new Error('Параметры запроса не соответсвуют схеме')
  }

  const userExists = await knex('accounts').where('login', login)

  if (userExists[0]) {
    res.status(400)
    throw new Error('Пользователь с данным логином уже существует')
  }

  const salt = await bcrypt.genSalt(10)
  const hashedPassword = await bcrypt.hash(password, salt)

  await knex('accounts').insert({
    idUser: userId,
    login: login,
    password: hashedPassword,
  })

  res.status(201).json({ message: 'Пользователь зарегистрирован успешно' })
})
