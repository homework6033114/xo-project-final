const asyncHandler = require('express-async-handler')
const knexConfig = require('../db/knexfile')
const knex = require('knex')(knexConfig[process.env.NODE_ENV])

// @desc   Создание сообщения в чате
// @route  POST /api/games/:gameId/chat
// @access Private
exports.addMessage = asyncHandler(async (req, res) => {
  // const { idGame, idUser, message, createdAt } = req.body

  if (!req.body) {
    res.status(405)
    throw new Error('Параметры запроса не соответсвуют схеме')
  }
  await knex('messenger').insert(req.body)
  res.status(200).json({ message: 'Успешно' })
})

// @desc   Получение сообщений из чата
// @route  GET /api/games/:gameId/chat
// @access Private
exports.getMessages = asyncHandler(async (req, res) => {
  const { gameId } = req.params

  if (!gameId) {
    res.status(404)
    throw new Error('Сообщения не найдены')
  }
  const messages = await knex('messenger')
    .where('idGame', gameId)
    .orderBy('createdAt', 'asc')

  res.status(200).json(messages)
})
