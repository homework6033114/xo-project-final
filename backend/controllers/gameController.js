//Homework-11 only
const asyncHandler = require('express-async-handler')
const knexConfig = require('../db/knexfile')
const knex = require('knex')(knexConfig[process.env.NODE_ENV])

const fs = require('fs')
const path = require('path')
const games = require('../dev-data/games.json')

const pathName = path.join(
  path.dirname(require.main.filename),
  'dev-data',
  'games.json'
)
const saveData = (data) => {
  fs.writeFile(pathName, JSON.stringify(data), (err) => {
    console.log(err)
  })
}

// @desc   Создание игрового поля
// @route  POST /api/game
// @access Private
const createField = asyncHandler(async (req, res) => {
  const { field } = req.body

  // const field = req.body

  if (!field) {
    res.status(405)
    throw new Error('Параметры запроса не соответсвуют схеме')
  }

  games.push(field)
  saveData(games)
  res.status(200).json({ message: 'Успешно' })
  // res.status(200).json(field)
})

// @desc   Получение игрового поля по id
// @route  GET /api/game
// @access Private
const getFieldByIdx = (req, res) => {
  const { fieldIdx } = req.body

  if (!games[fieldIdx]) {
    res.status(404)
    throw new Error(' Поле c данным индексом отсутсвует')
  }

  const field = games[fieldIdx]

  res.status(200).json(field)
}

// @desc   Обновление игрового поля
// @route  PUT /api/game
// @access Private
const updateFieldByIdx = (req, res) => {
  const { fieldIdx } = req.query
  const { updatedField } = req.body

  if (!games[fieldIdx]) {
    res.status(404)
    throw new Error(' Поле c данным индексом отсутсвует')
  }

  const updatedGames = [...games]
  updatedGames[fieldIdx] = updatedField
  saveData(updatedGames)

  res.status(200).json({ message: 'Успешно' })
  // res.status(200).json(updatedField)
}

// @desc   Удаление поля по индексу
// @route  DELETE /api/game
// @access Private
const deleteFieldByIdx = (req, res) => {
  const { fieldIdx } = req.query
  if (!games[fieldIdx]) {
    res.status(404)
    throw new Error(' Поле c данным индексом отсутсвует')
  }

  const updatedGames = games.filter((el) => el !== games[fieldIdx])
  saveData(updatedGames)
  res.status(200).json({ message: 'Успешно' })
}

module.exports = {
  createField,
  getFieldByIdx,
  updateFieldByIdx,
  deleteFieldByIdx,
}
