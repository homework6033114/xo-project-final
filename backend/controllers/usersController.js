const asyncHandler = require('express-async-handler')
const knexConfig = require('../db/knexfile')
const knex = require('knex')(knexConfig[process.env.NODE_ENV])

// @desc   Получение списка игроков
// @route  GET /api/users
// @access Private/admin
exports.getUsers = asyncHandler(async (req, res) => {
  const data = await knex('users')
    .limit(10)
    .where('isAdmin', false)
    .orderBy('id', 'asc')
  res.status(200).json(data)
})

// @desc   Получение списка активных игроков
// @route  GET /api/users
// @access Private
exports.getActivePlayers = asyncHandler(async (req, res) => {
  const data = await knex('users')
    .where('statusActive', true)
    .where('isAdmin', false)
    .orderBy('id', 'asc')
    .limit(10)
  res.status(200).json(data)
})

// @desc   Получение рейтинга игроков
// @route  GET /api/users/rating
// @access Private
exports.getRating = asyncHandler(async (req, res) => {
  const data = await knex('counters')
    .innerJoin('users', 'counters.user_id', 'users.id')
    .whereRaw(`period >= date_trunc('month', now() - interval '6 month')`)
    .where('users.statusActive', true)
    .select(
      'counters.user_id as userId',
      'users.firstName',
      'users.surName',
      'users.secondName',
      knex.raw(`SUM(wins_cnt + loses_cnt + draws_cnt) AS games_cnt`),
      knex.raw(`SUM(wins_cnt) AS wins_cnt`),
      knex.raw(`SUM(loses_cnt) AS loses_cnt`),
      knex.raw(
        `SUM(wins_cnt::float) / SUM(wins_cnt + loses_cnt + draws_cnt)*100 AS wins_perc`
      ),
      knex.raw(`SUM(wins_cnt * (CASE WHEN symbol = 'X' THEN 0.9 ELSE 1 END) -
   loses_cnt * (CASE WHEN symbol = 'X' THEN 1.1 ELSE 1 END) + draws_cnt * 0.25) /
   (COUNT(DISTINCT period)) AS rating`)
    )
    .groupBy('userId', 'users.firstName', 'users.surName', 'users.secondName')
    .having(knex.raw(`SUM(wins_cnt + loses_cnt + draws_cnt)`), '>', 50)
    .orderBy('rating', 'desc')
  // .limit(20)
  res.status(200).json(data)
})

// @desc   Получение рейтинга одного игрока
// @route  GET /api/users/rating/:userId
// @access Private
exports.getRatingById = asyncHandler(async (req, res) => {
  const { userId } = req.params
  const data = await knex('counters')
    .where('user_id', userId)
    .innerJoin('users', 'counters.user_id', 'users.id')
    .select(
      knex.raw(
        `SUM(wins_cnt::float) / SUM(wins_cnt + loses_cnt + draws_cnt)*100 AS wins_perc`
      )
    )

  res.status(200).json(data)
})

// @desc Добавление нового пользователя
// @route  POST /users
// @access Private (Admin)
exports.addUser = asyncHandler(async (req, res) => {
  const userData = req.body
  if (userData) {
    await knex('users').insert(userData)
    res.status(200).json({ message: 'Новый пользователь успешно добавлен' })
  } else {
    res.status(405)
    throw new Error('Параметры запроса не соответсвуют схеме')
  }
})

// @desc Обновление данных пользователя
// @route  PUT /users/:userId
// @access Private (Admin)
exports.updateUser = asyncHandler(async (req, res) => {
  const { userId } = req.params
  const updatedData = req.body

  if (userId && updatedData) {
    const user = await knex('users').where('id', userId).update(updatedData)

    if (!user) {
      res.status(404)
      throw new Error('Игрок не найден')
    }
    res.status(200).json({ message: 'Данные игрока успешно обновлены' })
  } else {
    res.status(405)
    throw new Error('Параметры запроса не соответсвуют схеме')
  }
})
