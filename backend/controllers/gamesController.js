const asyncHandler = require('express-async-handler')
const knexConfig = require('../db/knexfile')
const knex = require('knex')(knexConfig[process.env.NODE_ENV])

// @desc   Получение списка игр
// @route  GET /api/games
// @access Private
exports.getGames = asyncHandler(async (req, res) => {
  const games = await knex('games')
    .innerJoin('users as player1', 'games.idPlayer1', 'player1.id')
    .innerJoin('users as player2', 'games.idPlayer2', 'player2.id')
    .whereNotNull('gameEnd')
    .select(
      'games.id',
      'player1.surName as surName1',
      'player1.firstName as firstName1',
      'player1.secondName as secondName1',
      'player2.surName as surName2',
      'player2.firstName as firstName2',
      'player2.secondName as secondName2',
      'games.gameResult',
      'games.gameBegin',
      'games.gameEnd'
    )
    .orderBy('gameEnd', 'desc')
    .limit(15)

  res.status(200).json(games)
})

// @desc   Создание новой игры
// @route  POST /api/games
// @access Private
exports.createGame = asyncHandler(async (req, res) => {
  const { idPlayer1, idPlayer2, gameBegin } = req.body

  if (!idPlayer1 || !idPlayer2 || !gameBegin) {
    res.status(405)
    throw new Error('Параметры запроса не соответсвуют схеме')
  }

  const gameId = await knex('games')
    .insert({
      idPlayer1,
      idPlayer2,
      gameBegin,
    })
    .returning('id')

  res.status(200).json(gameId)
})

// @desc  Получение информации о текущей игре
// @route  GET /api/games/:gameId
// @access Private
exports.getGame = asyncHandler(async (req, res) => {
  const { gameId } = req.params

  if (!gameId) {
    res.status(404)
    throw new Error(' Игра c данным id отсутсвует')
  }

  const game = await knex('games')
    .where('games.id', gameId)
    .innerJoin('users as player1', 'games.idPlayer1', 'player1.id')
    .innerJoin('users as player2', 'games.idPlayer2', 'player2.id')
    .select(
      'games.id',
      'player1.id as playerId1',
      'player2.id as playerId2',
      'player1.surName as surName1',
      'player1.firstName as firstName1',
      'player1.secondName as secondName1',
      'player2.surName as surName2',
      'player2.firstName as firstName2',
      'player2.secondName as secondName2'
    )
  res.status(200).json(game)
})

// @desc  Обновление игры
// @route  PUT /games
// @access Private
exports.updateGame = asyncHandler(async (req, res) => {
  const { gameEnd, gameResult } = req.body
  const { gameId } = req.params

  const game = await knex('games').where('id', gameId).update({
    gameEnd,
    gameResult,
  })

  if (!game) {
    res.status(404)
    throw new Error('Игра не найдена')
  }
  res.status(200).json({ message: 'Данные игры обновлены успешно' })
})

// @desc   Получение данных игрового поля
// @route  GET /api/games/:gameId/moves
// @access Private
exports.getMoves = asyncHandler(async (req, res) => {
  const { gameId } = req.params

  if (!gameId) {
    res.status(404)
    throw new Error('Данные не найдены')
  }
  const moves = await knex('moves')
    .where('game_id', gameId)
    .select('position', 'symbol')
    .orderBy('id', 'asc')

  res.status(200).json(moves)
})
