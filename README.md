# Приложение Крестики-нолики

## Данные пользователей для входа в приложение:

### Игроки - статус пользователя - активен:

login: Gordeev  
password: 123456

login: Loginov  
password: 123456

### Игроки - статус пользователя - заблокирован:

login: Aleksandrov  
password: 123456

### Админ:

login: Andreeva  
password: 123456

## Запустить приложение:

```
# frontend (:3000) & backend (:5000)
npm run dev

# backend only
npm run server
```

## Env Variables

```
NODE_ENV = development
PORT = 5000
```

### Домашнее задание №15 Аналитическая обработка данных:

Seeder для генерации игр и пользователей - https://gitlab.com/homework6033114/xo-project-final/-/tree/main/backend/db/seeds

Миграция - XO_project-https://gitlab.com/homework6033114/xo-project-final/-/blob/main/backend/db/migrations/20230616062301_initRatingTable.js

SQL-запросы - https://gitlab.com/homework6033114/xo-project-final/-/tree/main/SQL
